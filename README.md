# Interface Board Embedded

Embedded solution that makes the entire vehicle/lidar system hot pluggable.  Credit card form factor.  Featuring a NXP MK20DX256VLK7 for vehicle controls, an NXP iMXRT1062DVJ6A for edge LiDAR processing, and a Microchip USB2517 for the hub.

![Tentative](/System Block Diagram.PNG "Block Diagram")